#!/usr/bin/python3

########################################
# Backup Script V1
# Written by:
# Mike Hall - mhall@sunitafe.edu.au
# Jacob Rollo - jacobrollo@hotmail.com
########################################

#Run the script with an argument that corrosponds to a string eg. backup01 in the arg variable to back up the directory it matches in the jobs list.

#VARIABLES

args = ['backup01','backup02','backup03','backup04'] #these are the variables that provide the location of the files or directory being backed up, and where to back them up to.

jobs = {
    "backup01": ['C:\Users\jacob\Desktop\TAFE','C:\Users\jacob\Desktop\TAFEBackup'], #This is a list that contains the directories we'll be backing up and where to back them up. 
    "backup02": ['C:\Users\jacob\Documents\Overwatch','C:\Users\jacob\Documents\OverwatchBackup'],
    "backup03": ['C:\Users\jacob\Documents\EmptyFolder','C:\Users\jacob\Documents\EmptyFolderBackup'],
    "backup04": ['C:\Users\jacob\Documents\My Pictures','C:\Users\jacob\Documents\PicturesBackup'], 
}

usage_message = "Usage: python3 backup.py <backup01|backup02|backup03>" #Usage message informing how to run the script.

logfile = '/home/mhall/backup.log' #it's where the logfile is. 

message = "" #initalises the message variable with nothing in the string..

errors = 0 #initalises the error variable at 0.

#IMPORTS

import sys, os, os.path, pathlib, shutil, smtplib   
from datetime import datetime
#sys is imported for sys.argv, so we can pull the arguments from the command line. os and os.path is used to determine if a directory exists. pathlib is used to determine whether we're copying a file or directory.
#shutil is used to copy the files. smtplib is used to send an email about the result of the script.
#datetime is used to grab the date and time for logging and naming purposes.

#FUNCTIONS

def do_logfile(job): #defines logfile function.

    global errors
    global message #these three lines are so we can modify our variables outside the scope of this function.
    global logfile

    now = datetime.now() # current date and time
    datestring = now.strftime("%Y%m%d-%H%m%s") #just grabbing the date and time and formatting it in a variable.
    #datestring = now.strftime("%Y%m%d-%H%I%S")

    if errors == 0:
        logmsg = datestring + " " + job + " SUCCESS" #this saves a log of the date and time, the job performed and that it was a success.

    else:
        logmsg = datestring + " " + job + " " + message + " FAIL" #this saves a log of the date and time, the job performed and that it was a failure.

    file = open(logfile,"a") 
    file.write(logmsg + "\n") 
    file.close() #these past three lines open the log file, write the message, and close it.


def do_backup(job): #defines our backup function. (job) is a substitute used to refer the the argument we'll be providing within the scope of the function.

    global message #allowing the variables to be modified outside the functions scope.
    global errors

    src = jobs[job][0] #this sets the src and dst variable using the table at the beginning of the script that contains the backup directories.
    dst = jobs[job][1]

    if not os.path.exists(src):
        message += src + " does not exist " 
        errors += 1 #these check if the src and dst directories exist. If it doesn't, it'll modify the message variable to say so, and add an error count.

    if not os.path.exists(dst):
        message += dst + " does not exist "
        errors += 1  

    if errors == 0: #if there hasn't be any errors, run the code below.

        now = datetime.now() # current date and time
        datestring = now.strftime("%Y%m%d-%H%I%S")

        p = pathlib.Path(src)
        is_a_file = p.is_file() #just defining some variables to be some commands that check if the src is a file or directory.
        is_a_dir = p.is_dir()

        path = pathlib.PurePath(src) #assigns the variable path to the src parent directory.
        dstpath = dst + "/" + path.name + "-" + datestring #assigns dstpath to be the backup destination.

        if is_a_file == True: #if it's a file, we try copying them. If that doesn't work, it provides an error message.
            try:
                shutil.copy2(src, dstpath)
            except:
                message += "Backup job " + job + " failed."
                errors += 1 

        elif is_a_dir == True: #if it's a directory, we try copying them. If that doesn't work, it provides an error message.
            try:
                shutil.copytree(src, dstpath)
            except:
                message += "Backup job " + job + " failed."
                errors += 1 


def do_email():
#here we define a function to send an email on the event of the backup failing.
    global message

    to         = 'jacobrollo@hotmail.com'
    gmail_user = 'jacobrollo@gmail.com'
    gmail_pwd  = 'xxxxxxxx' #past three lines define who we're sending the email to, and which email account we're sending it from.
    smtpserver = smtplib.SMTP("smtp.gmail.com",587) #defines the server we're logging into.
    smtpserver.ehlo() #we're identifying ourselves to the server.
    smtpserver.starttls() #this turns an insecure connection into a secure one.
    smtpserver.login(gmail_user, gmail_pwd) #logs into the email we're using to send the backup result.
    header     = 'To:' + to + '\n' + 'From: ' + gmail_user + '\n' + 'Subject: Backup Error \n' #covers the header format of emails.
    msg        = header + '\n' + message + ' \n\n' #defines the message.
    smtpserver.sendmail(gmail_user, to, msg) #sends the email.
    smtpserver.quit() #quits the emai server.

#ALGORITHM

if len(sys.argv) == 2: #if the length of the arguments provided equals 2, proceed to run the script. Otherwise, provide a usage message.

    script, arg = sys.argv #define two variables - the name of the script, and the argument provided.

    if arg in args: #if the argument is in the list of backup directories, proceed.

        do_backup(arg) #calls upon the do_backup function to do our backup, providing the variable arg as it's argument.

        if errors > 0: #if the error count is over 0, send an email.

            do_email()

        do_logfile(arg)#calls upon the logfile function to log the result.

    else: #if the argument isn't in the list, display the usage message.

        print(usage_message)

else:

    print(usage_message)
